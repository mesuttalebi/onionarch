﻿using System.Web.Mvc;

namespace OnionArch.Presentation.Controllers
{
    using OnionArch.BusinessService.Interfaces;

    public class HomeController : Controller
    {
        private readonly IBlogService _blogService;

        public HomeController(IBlogService blogService)
        {
            this._blogService = blogService;
        }

        public ActionResult Index()
        {
            var blogs = this._blogService.GetAll;
            return View(blogs);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}