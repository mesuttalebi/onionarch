﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OnionArch.Presentation.Startup))]
namespace OnionArch.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
