﻿namespace OnionArch.DAL
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using OnionArch.Domain;
    using OnionArch.Domain.Entities;
    using OnionArch.Presentation.Models;
    using System.Data.Entity;

    /// <summary>
    /// The application db context.
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        


        // DbSets
        public DbSet<Blog> Blogs { get; set; }
    }
}