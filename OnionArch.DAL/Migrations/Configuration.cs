namespace OnionArch.DAL.Migrations
{
    using OnionArch.Domain.Entities;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<OnionArch.DAL.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "OnionArch.DAL.ApplicationDbContext";
        }

        protected override void Seed(OnionArch.DAL.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Blogs.AddOrUpdate(x=> x.Name, new [] {
                new Blog() { Name= "ABC Blog" },
                new Blog() { Name= "0 Blog" },
                new Blog() { Name= "! Blog" },
                new Blog() { Name= "2 Blog" },
                new Blog() { Name= "3 Blog" },
                new Blog() { Name= "4 Blog" },
                new Blog() { Name= "BC Blog" },
                new Blog() { Name= "2BC Blog" },
                new Blog() { Name= "AC Blog" },
            });

            context.SaveChanges();
        }
    }
}
