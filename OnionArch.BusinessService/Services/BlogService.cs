﻿using System.Collections.Generic;

namespace OnionArch.BusinessService.Services
{
    using OnionArch.BusinessService.Interfaces;
    using OnionArch.DAL;
    using OnionArch.Domain.Entities;
    using System.Linq;

    public class BlogService: IBlogService
    {
        public IEnumerable<Blog> GetAll
        {
            get
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Blogs.ToList();
                }
            }
        }
    }
}
