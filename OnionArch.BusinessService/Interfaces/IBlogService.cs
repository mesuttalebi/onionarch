﻿namespace OnionArch.BusinessService.Interfaces
{
    using OnionArch.Domain.Entities;
    using System.Collections.Generic;

    public interface IBlogService
    {
        IEnumerable<Blog> GetAll { get; }
    }
}
