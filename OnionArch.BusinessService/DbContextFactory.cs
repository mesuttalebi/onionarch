﻿namespace OnionArch.BusinessService
{
    using OnionArch.DAL;
    using OnionArch.Domain;

    public static class DbContextFactory 
    {
        public static IDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
